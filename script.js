var toolType = 1;
var	r2 = 0;
var	g2 = 0;
var	b2 = 0;
var c = [r2,g2,b2];

function setup()
{
	var canvasDiv = document.getElementById("canvas");
	const styleElement = getComputedStyle(document.getElementById("canvas"));
	var height = parseInt(styleElement.height);
	var width = parseInt(styleElement.width);
	
	var myCanvas = createCanvas(width,height);
	myCanvas.parent('canvas');
}



function draw()
{

		switch(toolType){
			case 1:
				stroke(c);
				if(mouseIsPressed && mouseX>(window.screen.width-width)/2){
					strokeWeight(document.getElementById("range").value);
					line(mouseX, mouseY, pmouseX, pmouseY);
				}
			break;

			case 2:
				erase();
				if(mouseIsPressed && mouseX>(window.screen.width-width)){
					strokeWeight(document.getElementById("range").value);
					line(mouseX, mouseY, pmouseX, pmouseY);
				}
				noErase();
			break;

			case 3:
				if(mouseIsPressed && mouseX>(window.screen.width-width)){
					c = get(mouseX,mouseY);
					cursor('https://s3.amazonaws.com/mupublicdata/cursor.cur');
					toolType = 1;
				}
			break;

			case 5:
			    r2 = parseInt(document.getElementById("r1").value);
				g2 = parseInt(document.getElementById("g1").value);
				b2 = parseInt(document.getElementById("b1").value);
				console.log(r2,g2,b2);
				document.getElementById("ColorPreview").style.backgroundColor = 'rgb(' + [r2,g2,b2].join(',') + ')';
			break;

    } 
}


document.onclick = function(event) {
	if(event.target.innerText==='1'){
		toolType=1;
		console.log("Tool Selected: "+"Brush")
	}
	if(event.target.innerText==='2'){
		toolType=2;
		console.log("Tool Selected: "+"Rubber")
	}
	if(event.target.innerText==='3'){
		toolType=3;
		console.log("Tool Selected: "+"Color Selector")
		cursor(CROSS);
	}
	if(event.target.innerText==='4'){
		saveCanvas(canvas);
	}
	if(event.target.innerText==='5'){
		toolType = 5;
		document.getElementById("myColorPicker").style.display = "block";
		var span = document.getElementsByClassName("close")[0];
		span.onclick = function() {
		  	document.getElementById("myColorPicker").style.display = "none";
		  	c = [r2,g2,b2];
		  	toolType = 1;
		}
	}
}

